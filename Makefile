PY?=python3
PELICAN?=pelican
PELICANOPTS=

SASS?=sassc
SASSOPTS=-t compressed

BASEDIR=.
THEMESDIR=$(BASEDIR)/themes
THEMES=$(wildcard $(THEMESDIR)/*)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

SSH_HOST_PI=mars.perimeterinstitute.ca
SSH_USER_PI=smay
SSH_TARGET_DIR_PI=/nfs/unity/unixhome/smay/public_html

SSH_HOST_MPA=slogin.mpa-garching.mpg.de
SSH_USER_MPA=smay
SSH_TARGET_DIR_MPA=/afs/mpa/home/smay/public_html


DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

RELATIVE ?= 0
ifeq ($(RELATIVE), 1)
	PELICANOPTS += --relative-urls
endif

help:
	@echo 'Makefile for a pelican Web site                                           '
	@echo '                                                                          '
	@echo 'Usage:                                                                    '
	@echo '   make html                           (re)generate the web site          '
	@echo '   make clean                          remove the generated files         '
	@echo '   make regenerate                     regenerate files upon modification '
	@echo '   make publish_{pi,mpa}               generate using production settings '
	@echo '   make serve [PORT=8000]              serve site at http://localhost:8000'
	@echo '   make serve-global [SERVER=0.0.0.0]  serve (as root) to $(SERVER):80    '
	@echo '   make devserver [PORT=8000]          serve and regenerate together      '
	@echo '   make ssh_upload_{pi,mpa}            upload the web site via SSH        '
	@echo '   make rsync_upload_{pi,mpa}          upload the web site via rsync+ssh  '
	@echo '                                                                          '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html   '
	@echo 'Set the RELATIVE variable to 1 to enable relative urls                    '
	@echo '                                                                          '
SASS_FILES:=$(foreach dir,$(THEMES),$(wildcard $(dir)/static/sass/*.scss))
SASS_DIRS:=$(dir $(SASS_FILES))
SASS_NAMES:=$(notdir $(SASS_FILES))
CSS_DIRS:=$(SASS_DIRS:/sass/=/css/)
CSS_NAMES:=$(SASS_NAMES:.scss=.css)
CSS_FILES:=$(join $(CSS_DIRS),$(CSS_NAMES))

define css_rule =
sass_file:=$(1)
sass_dir:=$$(dir $$(sass_file))
sass_name:=$$(notdir $$(sass_file))
css_file:=$$(sass_dir:/sass/=/css/)$$(sass_name:.scss=.css)
$$(css_file): $$(sass_file) $$(sass_dir)lib/_*.scss
	$(SASS) $(SASSOPTS) $$< $$@
endef

$(foreach sass_file,$(SASS_FILES),$(eval $(call css_rule,$(sass_file))))

css: $(CSS_FILES)

arxiv-data-js:
	curl -O --output-dir content/js/ https://arxiv.org/a/0000-0002-2781-6304.json

html: css arxiv-data-js
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

clean:
	sed -i "s/^SITEURL = .*/SITEURL = '<PLACEHOLDER>'/" $(PUBLISHCONF)
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)

regenerate:
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
ifdef PORT
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

serve-global:
ifdef SERVER
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT) -b $(SERVER)
else
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT) -b 0.0.0.0
endif


devserver:
ifdef PORT
	$(PELICAN) -lr $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	$(PELICAN) -lr $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

publish_pi: css arxiv-data-js
	sed -i "s|^SITEURL = .*|SITEURL = '/personal/smay'|" $(PUBLISHCONF)
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)
	sed -i "s/^SITEURL = .*/SITEURL = '<PLACEHOLDER>'/" $(PUBLISHCONF)

publish_mpa: css arxiv-data-js
	sed -i "s|^SITEURL = .*|SITEURL = '/~smay'|" $(PUBLISHCONF)
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)
	sed -i "s/^SITEURL = .*/SITEURL = '<PLACEHOLDER>'/" $(PUBLISHCONF)

ssh_upload_mpa: publish_mpa
	scp -r $(OUTPUTDIR)/* $(SSH_USER_MPA)@$(SSH_HOST_MPA):$(SSH_TARGET_DIR_MPA)

rsync_upload_mpa: publish_mpa
	rsync -e "ssh" -P -rvzc --cvs-exclude --delete $(OUTPUTDIR)/ $(SSH_USER_MPA)@$(SSH_HOST_MPA):$(SSH_TARGET_DIR_MPA)

ssh_upload_pi: publish_pi
	scp -r $(OUTPUTDIR)/* $(SSH_USER_PI)@$(SSH_HOST_PI):$(SSH_TARGET_DIR_PI)

rsync_upload_pi: publish_pi
	rsync -e "ssh" -P -rvzc --cvs-exclude --delete $(OUTPUTDIR)/ $(SSH_USER_PI)@$(SSH_HOST_PI):$(SSH_TARGET_DIR_PI)


.PHONY: css html help clean regenerate serve serve-global devserver stopserver publish ssh_upload_mpa rsync_upload_mpa ssh_upload_pi rsync_upload_pi
