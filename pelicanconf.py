#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Simon May'
SITENAME = 'Simon May'
SITESUBTITLE = 'Theoretical astro- & particle physicist, computational cosmologist'
#SITEURL = ''
THEME = 'themes/storm_mod'
METADATA = 'Simon May’s personal homepage'
FAVICON = True

PATH = 'content'
STATIC_PATHS = ['files', 'js']
EXTRA_PATH_METADATA = {'files/favicon.ico': {'path': 'favicon.ico'},}

TIMEZONE = 'America/Toronto'

DEFAULT_LANG = 'en'
# take default date form file system
DEFAULT_DATE = 'fs'

# page settings
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
# disable creation of author, category and tag pages
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
TAG_SAVE_AS = ''
TAGS_SAVE_AS = ''
# disable archives page
ARCHIVES_SAVE_AS = ''
# disable creation of list of articles/pages
INDEX_SAVE_AS = ''

# disable feeds
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
# disable menu for now
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

# Blogroll
LINKS = ()

# Social widget
SOCIAL = (('arXiv', 'https://arxiv.org/a/0000-0002-2781-6304'),
          ('ADS', 'https://ui.adsabs.harvard.edu/search/q=orcid%3A0000-0002-2781-6304&sort=date%20desc%2C%20bibcode%20desc'),
          ('INSPIRE', 'https://inspirehep.net/authors/1713000'),
          ('ORCID', 'https://orcid.org/0000-0002-2781-6304'),
          ('GitLab', 'https://gitlab.com/Socob'),
          ('E-Mail', 'mailto:simon.may@pitp.ca'),
          ('PGP', 'files/0x1A6C70B821CE8D97-public.asc'),
)

DEFAULT_PAGINATION = False

#RELATIVE_URLS = True
