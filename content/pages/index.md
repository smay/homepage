Title: About me
Save_as: index.html
Url: 
JS: js/myarticles_settings.js, js/myarticles.js

Hi, my name is [**Simon May**](mailto:simon.may@pitp.ca). I’m doing research in theoretical astro- and particle physics, mainly driven by one question: *What is dark matter?*

I’m currently a Postdoctoral Fellow at the [Perimeter Institute for Theoretical Physics](https://perimeterinstitute.ca/).

Previously, I finished my PhD (Dr. rer. nat.), advised by [Volker Springel](https://www.mpa-garching.mpg.de/members/55019), at the [Max Planck Institute for Astrophysics](https://www.mpa-garching.mpg.de/) with a dissertation about *Structure formation in quantum-wave dark matter cosmologies*.

I obtained my master’s degree in physics in 2018 at [WWU Münster (Germany)](https://www.uni-muenster.de/en/), where I wrote my master’s thesis [*Minimal dark matter models with radiative neutrino masses: From Lagrangians to observables*](https://arxiv.org/abs/2003.04157).
My advisor was [Michael Klasen](https://www.uni-muenster.de/Physik.TP/en/research/klasen/).
As a result, I won the [Infineon Master Award](https://www.uni-muenster.de/Physik/en/department/infineon.html) for the best master’s thesis of the year within WWU Münster’s Department of Physics.
I spent one year at [Lund University (Sweden)](https://www.lunduniversity.lu.se/) during my master’s program as part of the EU Erasmus exchange program.

My bachelor’s thesis was on [*Properties of supersymmetric top quark pair-production at hadron colliders*](https://www.uni-muenster.de/imperia/md/content/physik_tp/theses/kulesza/simon_may_bsc.pdf), with [Anna Kulesza](https://www.uni-muenster.de/Physik.TP/en/research/kulesza/) as my advisor.

## Publications
My publications can be found on [arXiv](https://arxiv.org/a/0000-0002-2781-6304) (other databases: [ADS](https://ui.adsabs.harvard.edu/search/q=orcid%3A0000-0002-2781-6304&sort=date%20desc%2C%20bibcode%20desc), [INSPIRE](https://inspirehep.net/authors/1713000), [ORCID](https://orcid.org/0000-0002-2781-6304)).

<div id="arxivfeed"></div>

## Talks
Here is a chronological list of scientific talks which I have given (slightly outdated; will be updated soon).

- [*Cosmological fuzzy dark matter simulations in large(r) volumes*](https://wwwmpa.mpa-garching.mpg.de/darkmatter/DM_DAY_MPA.html).  
  [Dark Matter Day: Mini-Workshop on Dark Matter](https://wwwmpa.mpa-garching.mpg.de/darkmatter/DM_DAY_MPA.html) (Max Planck Institute for Astrophysics, Garching, Germany), 30th October 2020.
- [*Cosmological simulations of structure formation with fuzzy dark matter*]({static}/files/2020-09-21_talk.pdf).  
  [MPA Institute Seminar](https://www.mpa-garching.mpg.de/498221/MPA-Institute-Seminar) (Max Planck Institute for Astrophysics, Garching, Germany), 21st September 2020.
- [*Cosmological fuzzy dark matter simulations: differences to cold dark matter*]({static}/files/2020-07-31_talk.pdf).  
  [9th IMPRS Student Symposium](https://www.imprs-astro.mpg.de/sites/default/files/9th_symposium.jpg) (Garching, Germany), 31st July 2020.
- [*Challenges and limitations of fuzzy dark matter simulations*]({static}/files/2020-05-06_talk.pdf).  
  Galaxies & Simulations meeting (Max Planck Institute for Astrophysics, Garching, Germany), 6th May 2020.
- [*Challenges of cosmological fuzzy dark matter simulations*](https://wwwmpa.mpa-garching.mpg.de/darkmatter/slides_speakers_2020/dark_matters_2020-01-29_Simon_May.pdf).  
  [Munich/Garching Dark Matter Meeting](https://wwwmpa.mpa-garching.mpg.de/darkmatter/) (Munich, Germany), 29th January 2020.
- [*Triplet scalar and singlet–doublet fermion dark matter in a radiative model of neutrino masses*](https://www.dpg-verhandlungen.de/year/2019/conference/aachen/part/t/session/64/contribution/5).  
  [DPG spring meeting 2019](https://aachen19.dpg-tagungen.de/) (Aachen, Germany), 27th March 2019.
- [*Singlet–doublet/triplet dark matter and neutrino masses*](http://moriond.in2p3.fr/2019/EW/slides/4_Wednesday/3_YSF/3_simon_may_moriond_talk_handout.pdf).  
  [54th Rencontres de Moriond on Electroweak Interactions and Unified Theories](http://moriond.in2p3.fr/2019/EW/) (La Thuile, Italy), 20th March 2019.
- [*Fuzzy dark matter*](https://www.lorentzcenter.nl/lc/web/2018/1070/presentations/Simon%20May.pdf).  
  [Workshop on “Computational Cosmology”](https://www.lorentzcenter.nl/lc/web/2018/1070/info.php3?wsid=1070) (Lorentz Center, Leiden, Netherlands),
6th December 2018.
- [*Minimal dark matter models with radiative neutrino masses: From Lagrangians to observables*](https://www.uni-muenster.de/Physik.TP/en/teaching/courses/research_seminar_quantum_field_theory_ss2018.html).  
  Research seminar on quantum field theory (Institute of Theoretical Physics, WWU
Münster, Germany), 25th June 2018.
- [*Minimal extensions of the Standard Model explaining dark matter and neutrino masses*](https://www.uni-muenster.de/Physik.TP/en/teaching/courses/research_seminar_quantum_field_theory_ws2017-2018.html).  
  Research seminar on quantum field theory (Institute of Theoretical Physics, WWU Münster), 27th November 2017.
